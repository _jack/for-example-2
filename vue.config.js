// vue.config.js
const BundleTracker = require("webpack-bundle-tracker");
const path = require('path')
function resolve (dir) {
  return path.join(__dirname, dir)
}
module.exports = {
  publicPath: "http://localhost:8080/",
  outputDir: './dist/',
  chainWebpack: config => {
    config.optimization
      .splitChunks(false);
    config
      .plugin('BundleTracker')
      .use(BundleTracker, [{filename: '../client/webpack-stats.json'}]);
    config.resolve.alias
      .set('__STATIC__', 'static')
      .set('@', resolve('src'));
    config.devServer
      .public('http://localhost:8080')
      .host('localhost')
      .port(8080)
      .hotOnly(true)
      .watchOptions({poll: 1000})
      .https(false)
      .headers({"Access-Control-Allow-Origin": ["*"]})
  },
  css: {
    loaderOptions: {
      // передача настроек в sass-loader
      sass: {
        // @/ это псевдоним к каталогу src/ поэтому предполагается,
        // что у вас в проекте есть файл `src/variables.scss`
        data: `@import "~@/assets/sass/main.sass";`
      },
    }
  }
};
