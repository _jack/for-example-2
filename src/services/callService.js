import api from '@/services/api'

export default {
  validateUsername(params) {
    return api.call('/user/account#validateUsername', {
      'userName': params
    });
  },
  validateEmail(params) {
    return api.call('/user/account#validateEmail', {
      'email': params
    });
  },
  getPasswordPolicy() {
    return api.call('/user/pwd#getPolicy');
  },
  getCurrencies() {
    return api.call('/user/account#getCurrencies');
  },
  getCountries() {
    return api.call('/user/account#getCountries');
  },
  getSessionInfo() {
    return api.call('/user#getSessionInfo');
  },
  accountRegister(params) {
    return api.call('/user/account#register', params);
  },
  accountActivate(id) {
    return api.call('/user/account#activate', {
      "verificationCode": id
    });
  },
  userLogin(params) {
    return api.call('/user#login', {
      "usernameOrEmail": params.usernameOrEmail,
      "password": params.password,
    })
  },
  userLogout() {
    return api.call('/user#logout', {});
  },
  sendResetPwdEmail(params) {
    return api.call('/user/pwd#sendResetPwdEmail', {
      "email": params.email,
      "changePwdURL": params.pwdUrl,
      "captchaPublicKey": params.sitekey,
      "captchaResponse": params.captchaResponse
    })
  },
  isResetPwdKeyAvailable(key) {
    return api.call('/user/pwd#isResetPwdKeyAvailable', {
      "key": key
    })
  },
  resetPassword(params){
    return api.call('/user/pwd#reset', {
      "key": params.key,
      "password": params.password
    })
  }
}