import api from '@/services/api';
export default {
  sessionStateChange(onSubscribedCallback, onSignInCallback, onSignOutCallback) {
    api.subscribe('/sessionStateChange', (url, stateChange) => {
      switch (stateChange.code) {
        case 0: //Session is logged-in.
          onSignInCallback();
          break;
        case 1: //Session is expired.
        case 2: //Session is logged off
        case 3: //Session is terminated because another login occurs
        case 5: // Session is terminated because pre-set limitation time is reached.
        case 6: // Session is terminated because self-exclusion is enabled
          console.log(stateChange.code);
          onSignOutCallback(stateChange.desc);
          break;
        default: //Session default
          console.error(`EM Session state change Error unknown code ${stateChange.code}`);
      }
    }).then(() => {
      onSubscribedCallback();
      console.log('On session state change event was subscribed');
    }).catch((err) => {
      console.error(`EM Can't subsribe on session state change. Details: ${err}`);
    });
  }
}