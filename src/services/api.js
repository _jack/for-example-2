import autobahn from 'autobahn';

const url = 'wss://api-stage.everymatrix.com/v2';
const realm = 'http://www.flipperflip.com';

let config = {
  url: url,
  realm: realm
};

let connection;

export default (function() {

  function getConnection() {
    if (!connection) {
        connection = new Promise(function(resolve, reject) {
          if (!config.url || !config.realm) {
            return reject("Missing connection config url or realm, please use EMWebApi.setConfig to define them")
          }

          connection = new autobahn.Connection(config);

          connection.onopen = resolve;

          connection.onclose = function(reason, details) {
            connection = false;
            reject({
              reason: reason,
              details: details
            });
          };

          connection.open();
        })
      }
    return connection;
  }

  function doRequest(url, params) {
    return getConnection().then(function(session) {
      return session.call(url, [], params).then(function(response) {
        let result = null;
        if (response) {
          result = response.kwargs;
        }
        return result;
      }).catch(function(err) {
        const errorDetails = err && err.kwargs;
        return Promise.reject(errorDetails);
      });
    })
  }

  return {
    call: doRequest,
    getConnect: getConnection,
    subscribe: function(topic, callback) {
      return getConnection().then(function(session) {
        session.subscribe(topic, callback);
      });
    },
    unsubscribe: function(subscription) {
      return getConnection().then(function(session) {
        session.unsubscribe(subscription);
      });
    }
  }
})();
