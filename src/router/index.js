import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store/index'

import guest from './middleware/guest'
import auth from './middleware/auth'
import middlewarePipeline from './middlewarePipeline'

Vue.use(Router)

const router = new Router({
  // mode: 'history',
  // base: process.env.BASE_URL,
  base: '/',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        middleware: [
          guest
        ]
      },
      children: [
        {
          path: '/auth/:name',
          name: 'auth',
          component: Home,
          meta: {
            middleware: [
              guest
            ]
          },
        },
      ]
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
      path: '/activation&key=:id',
      name: 'activation',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/Activation.vue')
    },
    {
      path: '/resetpwd&key=:id',
      name: 'resetpwd',
      component: () => import(/* webpackChunkName: "about" */ '../views/CheckResetPassKey.vue')
    },
    {
      path: '/account',
      name: 'account',
      component: () => import(/* webpackChunkName: "account" */ '../views/Account.vue'),
      meta: {
        middleware: [
          auth
        ]
      }
    },
    {
      path: '*',
      name: 'NotFound',
      component: () => import(/* webpackChunkName: "about" */ '../views/404.vue')
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next()
  }
  const middleware = to.meta.middleware
  const context = {
    to,
    from,
    next,
    store
  }
  return middleware[0]({
    ...context,
    next: middlewarePipeline(context, middleware, 1)
  })
});

export default router;
