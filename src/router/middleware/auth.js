export default function auth({ next, store }) {
	if(!store.getters.getSessionInfo.isAuthenticated){
		return next({
			name: 'home'
		})
	}
	return next()
}