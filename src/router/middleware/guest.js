export default function guest({ next, store }) {
	if(store.getters.getSessionInfo.isAuthenticated) {
		return next({
			name: 'account'
		})
	}
	return next();
}