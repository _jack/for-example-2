import Vue from 'vue';
import App from './App.vue';
import Vuelidate from 'vuelidate';
import router from './router/index';
import store from './store/index';
import Notifications from 'vue-notification';

Vue.config.productionTip = false;

Vue.use(Vuelidate);
Vue.use(Notifications);

new Vue({
  router,
  store,
  render: h => h(App),
  provide: {
    stopSkroll (isOpen) {
      const body = document.body;
      if (isOpen) {
        body.classList.add('modal-open')
      } else {
        body.classList.remove('modal-open')
      }
    }
  }
}).$mount('#app');
