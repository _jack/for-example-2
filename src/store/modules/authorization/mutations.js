export const UPDATE_AUTH_COMPONENT = (state, payload) => {
  state.authorization.isShown = payload.isShown ? payload.isShown : false;
  state.authorization.componentName = payload.name ? payload.name : '';
};