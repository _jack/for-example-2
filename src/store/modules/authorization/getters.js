export const getAuthComponentName = state => state.authorization.componentName;
export const getAuthShowParameter = state => state.authorization.isShown;