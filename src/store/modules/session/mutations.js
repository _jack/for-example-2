export const UPDATE_SESSION = (state, payload) => {
	state.session = Object.assign({}, state.session, payload);
};
export const RESET_SESSION = (state) => {
	state.session = {
    isAuthenticated: false
  };
};

