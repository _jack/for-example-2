import Vue from 'vue';
import router from '@/router/index';
import call from '@/services/callService';
import subscribe from '@/services/subscribeService';

export const GET_SESSION_INFO = async ({ commit }) => {
  commit('UPDATE_SESSION', await call.getSessionInfo());
  refreshSessionStateChangeSubscribtion({ commit });
};

//call the sessionStateChange method to see all the status cases
let isSubscribed = false;
const refreshSessionStateChangeSubscribtion = ({ commit }) => {
  if (!isSubscribed) {
    subscribe.sessionStateChange(() => {
      isSubscribed = true;
    },() => {
      console.log('Sign In');
      Vue.notify({
        group: 'app',
        type:'success',
        text: 'Sign In'
      });
    }, (message) => {
      isSubscribed = false;
      console.log('Sign Out ', message);
      Vue.notify({
        group: 'app',
        type:'warn',
        text: message
      });
      commit('RESET_SESSION');
      commit('RESET_USER', null, { root: true });
      router.push('/');
    });
  }
};