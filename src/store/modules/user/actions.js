import call from '@/services/callService';
// TODO: Clear test data

export const ASYNC_CHANGE_USER = ({ commit }, payload) => {
	setTimeout(() => {
		// context.commit
		commit('CHANGE_USER_QUANTITY', payload.usersValue)
	}, payload.myTimer);
};
export const USER_LOGIN = async ({ dispatch, commit }, params) => {
	commit('UPDATE_USER', await call.userLogin(params));
	await dispatch('GET_SESSION_INFO', null, { root: true });
};