import Vue from 'vue';
import Vuex from 'vuex';
import userModule from './modules/user/index';
import sessionModule from './modules/session/index';
import authModule from './modules/authorization/index';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  plugins: [createPersistedState()],
  modules: {
    userModule,
    sessionModule,
    authModule
  },

  state: {
    clicks: 0
  },
  mutations: {

  },
  actions: {

  }
});